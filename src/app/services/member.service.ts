import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMember, RegisterModel, LoginModel } from './../interfaces/member.interface';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class MemberService {
    constructor(
        private httpClient: HttpClient
    ) { }

    private memberItems: IMember[] = [];
    private memberLogin: IMember = JSON.parse(localStorage.getItem('memberLogin') || null);
    private address: string = 'http://localhost:3000/api/members';

    /** สำหรับแสดงข้อมูลสมาชิกทั้งหมด */
    getMembers() {
        return this.httpClient
            .get<IMember[]>(this.address);
    }

    /** สำหรับสมัครสมาชิก */
    onRegister(value: RegisterModel) {
        return new Observable(observ => {
            this.httpClient
                .get<{ count: number }>(`${this.address}/count?where[username]=${value.username}`)
                .subscribe(res => {
                    // เช็คค่าซ้ำ
                    if (res.count != 0)
                        return observ.error({ message: 'มีผู้ใช้งานนี้ในระบบแล้ว!' });

                    // สร้าง Model ใหม่
                    const model: IMember = {
                        firstname: value.firstname,
                        lastname: value.lastname,
                        username: value.username,
                        password: value.password
                    };

                    // บันทึกข้อมูลจริงไปยัง API
                    this.httpClient
                        .post(this.address, model)
                        .subscribe(
                            res => observ.next(res),
                            err => observ.error(err))
                });

        });
    }

    /** สำหรับการเข้าสู่ระบบ */
    onLogin(value: LoginModel) {
        return new Observable(observ => {
            this.httpClient
                .get<IMember>(`${this.address}/findOne?filter[where][username]=${value.username}&filter[where][password]=${value.password}`)
                .subscribe(
                    res => {
                        this.memberLogin = res;
                        localStorage.setItem('memberLogin', JSON.stringify(this.memberLogin));
                        observ.next(this.memberLogin);
                    },
                    err => observ.error({ message: 'ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง !!!' }));
        });
    }

    /** ดึงข้อมูลสมาชิกที่เข้าสู่ระบบ */
    getMemberLogon(): Observable<IMember> {
        return new Observable<IMember>(observ => {
            observ.next(this.memberLogin);
        });
    }

    /** ออกจากระบบ เคลียค่า memberLogin */
    onLogout() {
        return new Observable(observ => {
            this.memberLogin = null;
            localStorage.removeItem('memberLogin');
            observ.next(this.memberLogin);
        });
    }
}
