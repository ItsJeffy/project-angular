export interface IjsonPlaceholder {
    userId: number;
    id: number;
    title: string;
}