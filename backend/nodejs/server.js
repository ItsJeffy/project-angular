const express = require('express');
const server = express();
const cors = require('cors');

/** Access-Control-Allow-Origin */
server.use(cors());

server.get('/', (request, response) => {
    const items = [
        {
            id: 100001,
            title: 'Connect Basic Angular 7 From NodeJS 01',
            userId: 10001
        },
        {
            id: 100002,
            title: 'Connect Basic Angular 7 From NodeJS 02',
            userId: 10002
        },
        {
            id: 100003,
            title: 'Connect Basic Angular 7 From NodeJS 03',
            userId: 10003
        }
    ];


    response.json(items);
});

server.listen(3000, () => console.log('NodeJS Server is started.'))