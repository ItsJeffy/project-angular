<?php
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

$items = array(
    array(
        'id'        => 10001,
        'title'     => 'Connect Basic Angular 7 From PHP 01',
        'userId'    => 101
    ),
    array(
        'id'        => 10002,
        'title'     => 'Connect Basic Angular 7 From PHP 02',
        'userId'    => 101
    ),
    array(
        'id'        => 10003,
        'title'     => 'Connect Basic Angular 7 From PHP 03',
        'userId'    => 102
    ),
    array(
        'id'        => 10004,
        'title'     => 'Connect Basic Angular 7 From PHP 04',
        'userId'    => 103
    ),
    array(
        'id'        => 10005,
        'title'     => 'Connect Basic Angular 7 From PHP 05',
        'userId'    => 103
    ),
);

echo json_encode($items);